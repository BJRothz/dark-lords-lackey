﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePlacer : MonoBehaviour {

    public GameObject BorderMountain;
    public GameObject Grassland;
    public GameObject Water;

    private Vector3 TilePos;

    private bool isOddRow;
    private int rowLength;
    private int columnLength;


    //initialisation
    void Start()
    {
        TilePos = new Vector3(-10, -5.25f, 0);

        isOddRow = false;
        rowLength = 21;
        columnLength = 14;

        for (int j = 0; j < columnLength; j++)
        {
            for (int i = 0; i < rowLength; i++)
            {
                if (i == 0 || j == 0 || i == rowLength - 1 || j == columnLength - 1)
                    Instantiate(BorderMountain, TilePos, Quaternion.identity);
                else
                    Instantiate(Grassland, TilePos, Quaternion.identity);

                TilePos.x++;
            }

            isOddRow = !isOddRow;
            if (isOddRow == false)
            {
                TilePos.x = -10;
                rowLength = 21;
            }
            else
            {
                TilePos.x = -10.5f;
                rowLength = 22;
            }
            TilePos.y = TilePos.y + 0.777f;
            TilePos.z++;
        }
    }

    // Called once per frame
    void Update ()
    {
		
	}
}
