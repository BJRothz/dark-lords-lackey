﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blackboard : MonoBehaviour
{
    public int sidesOfTile = 6;
    public bool isMapComplete = false;
    public bool isRegion1Complete = false;
    public bool mainMapGen = true;


    public IList<Tile> tiles = new List<Tile>();

    public GameObject grassTile;
    public GameObject hillyGrassTile;
    public GameObject waterTile;
    public GameObject mountainTile;
    public GameObject borderMountainTile;

    public int totalGrassTiles;
    public int totalHillyGrassTiles;
    public int totalWaterTiles;
    public int totalMountainTiles;
    public int totalBorderMountainTiles;

    public class Tile
    {
        Blackboard bb = new Blackboard();

        public bool isBlankTile;
        public bool isBorderTile;
        public bool side1Filled = false;
        public bool side2Filled = false;
        public bool side3Filled = false;
        public bool side4Filled = false;
        public bool side5Filled = false;
        public bool side6Filled = false;

        public bool[] sidesFilled;

        public int tilePrefab;
        public int tileID;

        public float x = 0;
        public float y = 0;
        public float z = 0;

        public Vector3 tilePos = new Vector3(0, 0, 0);

        GameObject thisTile;

        public void spawnTile(Blackboard bb)
        {
            //isBlankTile = fillBlank;

            tilePos = new Vector3(x, y, z);

            tilePrefab = Random.Range(0, 4);


            if (bb.mainMapGen)
            {
                switch (tilePrefab)
                {
                    case 0:
                        thisTile = Instantiate(bb.grassTile, tilePos, Quaternion.identity);
                        bb.totalGrassTiles++;
                        break;

                    case 1:
                        thisTile = Instantiate(bb.hillyGrassTile, tilePos, Quaternion.identity);
                        bb.totalHillyGrassTiles++;
                        break;

                    case 2:
                        thisTile = Instantiate(bb.waterTile, tilePos, Quaternion.identity);
                        bb.totalWaterTiles++;
                        break;

                    case 3:
                        thisTile = Instantiate(bb.mountainTile, tilePos, Quaternion.identity);
                        bb.totalMountainTiles++;
                        break;

                    /*case 4:
                        thisTile = Instantiate(bb.borderMountainTile, tilePos, Quaternion.identity);
                        bb.totalBorderMountainTiles++;
                        isBorderTile = true;
                        break;*/

                    default:
                        break;
                }
            }
            else
            {
                thisTile = Instantiate(bb.borderMountainTile, tilePos, Quaternion.identity);
                bb.totalBorderMountainTiles++;
                isBorderTile = true;
            }
        }
    }
}
