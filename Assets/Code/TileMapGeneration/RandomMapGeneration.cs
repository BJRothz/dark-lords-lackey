﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Blackboard))]

public class RandomMapGeneration : MonoBehaviour
{
    public Blackboard bb = new Blackboard();

    public GameObject test;


    // Use this for initialization
    void Start ()
    {
        bb.tiles.Add(new Blackboard.Tile());
        bb.tiles[0].spawnTile(bb);

        while (!bb.isMapComplete)
        {
            regionGeneration();
        }
	}
	
	// Update is called once per frame
	void Update ()
    {

    }

    //Gets total true values in a boolean array
    /*public bool shouldFillBlanks(int tileID)
    {
        bool fillBlanks = false;

        int totalTruths = 0;

        totalTruths = (bb.tiles[tileID].side1Filled ? 1:0) + (bb.tiles[tileID].side2Filled ? 1 : 0) + (bb.tiles[tileID].side3Filled ? 1 : 0) + 
                (bb.tiles[tileID].side4Filled ? 1 : 0) + (bb.tiles[tileID].side5Filled ? 1 : 0) + (bb.tiles[tileID].side6Filled ? 1 : 0);


        if (totalTruths >= 3)
        {
            fillBlanks = true;
        }

        return fillBlanks;
    }*/


    //checks the tile borders
    public void checkTileBorders(int TileID)
    {
        for (int i = 0; i < bb.tiles.Count; i++)
        {
            if (bb.tiles[TileID].tilePos + new Vector3(0.5f, 0.77f, 1) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side1Filled = true;
            }

            if (bb.tiles[TileID].tilePos + new Vector3(1, 0, 0) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side2Filled = true;
            }

            if (bb.tiles[TileID].tilePos + new Vector3(0.5f, -0.77f, -1) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side3Filled = true;
            }

            if (bb.tiles[TileID].tilePos + new Vector3(-0.5f, -0.77f, -1) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side4Filled = true;
            }

            if (bb.tiles[TileID].tilePos + new Vector3(-1, 0, 0) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side5Filled = true;
            }

            if (bb.tiles[TileID].tilePos + new Vector3(-0.5f, 0.77f, 1) == bb.tiles[i].tilePos)
            {
                bb.tiles[TileID].side6Filled = true;
            }
        }
    }

    //checks if the tile is bordered on al sides
    public bool checkTileCompletion( int tileID)
    {
        bool isTileComplete = false;

        if (bb.tiles[tileID].isBorderTile || bb.tiles[tileID].side1Filled & bb.tiles[tileID].side1Filled & bb.tiles[tileID].side1Filled & bb.tiles[tileID].side1Filled 
                & bb.tiles[tileID].side1Filled & bb.tiles[tileID].side1Filled)
        {
            isTileComplete = true;
        }

        return isTileComplete;
    }

   public void regionGeneration()
    {
        int thisLoopCount = bb.tiles.Count;

        for (int activeTileID = 0; activeTileID < thisLoopCount; activeTileID++)
        {
            if (!checkTileCompletion(activeTileID))
            {
                checkTileBorders(activeTileID);

                for (int i = 0; i < bb.sidesOfTile; i++)
                {
                    //bool fillBlanks = false;

                    /*if (bb.tiles[activeTileID].isBorderTile)
                    {
                        if (shouldFillBlanks(activeTileID))
                        {
                            fillBlanks = true;
                        }
                    }*/

                    //if (!bb.tiles[activeTileID].isBlankTile)
                    //{
                    switch (i)
                    {
                        case 0:
                            if (!bb.tiles[activeTileID].side1Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + 0.5f;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + 0.77f;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + 1;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        case 1:
                            if (!bb.tiles[activeTileID].side2Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + 1;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + 0;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + 0;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        case 2:
                            if (!bb.tiles[activeTileID].side3Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + 0.5f;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + -0.77f;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + -1;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        case 3:
                            if (!bb.tiles[activeTileID].side4Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + -0.5f;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + -0.77f;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + -1;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        case 4:
                            if (!bb.tiles[activeTileID].side5Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + -1;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + 0;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + 0;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        case 5:
                            if (!bb.tiles[activeTileID].side6Filled)
                            {
                                bb.tiles.Add(new Blackboard.Tile());
                                bb.tiles[bb.tiles.Count - 1].x = bb.tiles[activeTileID].x + -0.5f;
                                bb.tiles[bb.tiles.Count - 1].y = bb.tiles[activeTileID].y + 0.77f;
                                bb.tiles[bb.tiles.Count - 1].z = bb.tiles[activeTileID].z + 1;
                                bb.tiles[bb.tiles.Count - 1].spawnTile(bb);
                            }
                            break;
                        default:
                            break;
                    }
                    //}                      
                }
            }


        }

        if (bb.tiles.Count >= 50)
        {
            bb.mainMapGen = false;
        }

        if (bb.tiles.Count <= thisLoopCount)
        {
            bb.isMapComplete = true;
        }
    }
}
